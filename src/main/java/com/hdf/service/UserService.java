package com.hdf.service;

import com.hdf.entity.User;

public interface UserService {
    public void register(User user);
    User findByUserName(String username);

}
