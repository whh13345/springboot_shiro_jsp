package com.hdf.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

import java.util.Random;

public class SaltUtils {
    public static String getSalt(int n){
        char[] chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()".toCharArray();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<n;i++){
            char aChar = chars[new Random().nextInt(chars.length)];
            sb.append(aChar);
        }
    return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(getSalt(8));
        Md5Hash md5Hash=new Md5Hash("hdf","abc",2);
        System.out.println(md5Hash);
        System.out.println(md5Hash.toHex());
    }
}
